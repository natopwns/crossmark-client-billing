# json.py - Reading, parsing, and writing JSON files.

# Copyright (C) 2021  Nathan Taylor
# GPL-3.0-or-later


# External
import json


def read(json_file):

	with open(json_file, 'r') as opened_file:
		json_string = opened_file.read()       # convert file to a string
		json_data   = json.loads(json_string)  # parse json-formatted string

	return json_data

def write(input_data, f_name):

	if f_name[-5:] != '.json':
		f_name += '.json'

	with open(f_name, 'w', newline='') as new_file:
		# convert the input data to a json-formatted string
		json_string = json.dumps(
			input_data,
			indent=2,
			sort_keys=True
		)
		new_file.write(json_string)  # write the data to a file

