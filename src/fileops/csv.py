# csv.py - Reading and writing csvs.

# Copyright (C) 2021  Nathan Taylor
# GPL-3.0-or-later


# External
import csv


def read(csv_file):

	with open(csv_file, 'r') as imported_file:
		# read csv file
		reader = csv.reader(
			imported_file,
			delimiter=',',
			quotechar='"'
		)
		sheet  = list(reader)  # store info from csv.reader to a list

	return sheet

def write(input_list, f_name):

	if f_name[-4:] != '.csv':
		f_name += '.csv'

	with open(f_name, 'w', newline='') as new_file:
		writer = csv.writer(new_file, dialect='excel')
		writer.writerows(input_list)

