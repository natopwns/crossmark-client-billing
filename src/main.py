# main.py - Main controller.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import datetime
import calendar
import os
import fileops.csv
import fileops.json
import data.timesheets as timesheets
import clients
import config


# ---------
# VARIABLES
# ---------

is_new_project       = False
current_project_path = None


# ---------
# FUNCTIONS
# ---------

def check_path(path, name, ext):

	if ext == 'json':
		size = 5
	elif ext == 'csv':
		size = 4

	full_path = os.path.join(path, name)
	mod_path  = full_path

	valid = False
	number = 1

	while not valid:
		if os.path.exists(mod_path):
			mod_path = full_path[:-size] + '_' + str(number) + full_path[-size:]
			number += 1
		else:
			valid = True

	return mod_path

def load_timesheet(path):

	timesheet = fileops.csv.read(path)

	return timesheet

def run_report(report_type, path, timesheet):

	file_name = 'Report ' + report_type + '.csv'
	full_path = check_path(path, file_name, 'csv')

	report = timesheets.run_report(report_type, timesheet)

	fileops.csv.write(report, full_path)

def open_project(path):

	global is_new_project
	global current_project_path

	is_new_project = False
	current_project_path = path

	project = fileops.json.read(path)

	# load clients
	load_clients(project, from_open = True)
	config.save()

	return clients.Client

def new_project(project_path, timesheet_path):

	global is_new_project
	global current_project_path

	is_new_project = True

	# get timesheet data
	timesheet = fileops.csv.read(timesheet_path)
	timesheet_data = timesheets.process(timesheet)

	# create project
	project = create_project(timesheet_data)
	project_name = 'project_' + str(datetime.date.today()) + '.json'
	full_path = check_path(project_path, project_name, 'json')

	current_project_path = full_path

	fileops.json.write(project, full_path)

	# load clients
	load_clients(project)
	config.save()

	return clients.Client

def save_project():

	global is_new_project

	project = fileops.json.read(current_project_path)

	project['edited'] = {'clients': {}}

	for name, client in clients.Client.instances.items():

		settings = client.get_settings()
		data     = client.get_data()

		project['edited']['clients'][name] = {
			'date':     data['date'],
			'period':   data['period'],
			'memo':     data['memo'],
			'services': data['services'].copy(),
			'fee':      data['project_fee'],
			'expenses': data['expenses'].copy(),
			'total':    data['total']
		}

		# update config, if new project
		if is_new_project:
			config.update_client(name, settings, data)
			config.update_employees(data['services'].copy())

	if is_new_project:
		config.save()

	fileops.json.write(project, current_project_path)

def calc_client_totals(project):

	for section_name in project:

		section = project[section_name]['clients']

		for client_name in section:

			client = section[client_name]

			total = 0.0

			for line in client['services']:
				total += line[4]

			total += client['fee']

			for line in client['expenses']:
				total += line[1]
			
			client['total'] = round(total, 2)

def create_project(data):

	project = {
		'default': {'clients': {}},
		'edited':  {'clients': {}}
	}

	today = datetime.date.today()
	last_day = calendar.monthrange(today.year, today.month)[1]
	first = str(calendar.month_name[today.month]) + ' 1, ' + str(today.year)
	last  = str(calendar.month_name[today.month]) + ' ' + str(last_day) + ', ' + str(today.year)

	date   = str(today)[:-2] + str(last_day)
	period = 'Beginning ' + first + ' and Ending ' + last
	memo   = 'Consulting & Financial Management Services'

	for client in data:

		# get client config
		if client in config.settings['clients']:
			settings = config.settings['clients'][client]
			fee      = settings['fee']
			expenses = settings['expenses'].copy()
		else:
			fee      = 0.00
			expenses = []

		services = data[client]

		project['default']['clients'][client] = {
			'date':     date,
			'period':   period,
			'memo':     memo,
			'services': services,
			'fee':      fee,
			'expenses': expenses,
			'total':    0.0
		}
		project['edited']['clients'][client] = {
			'date':     date,
			'period':   period,
			'memo':     memo,
			'services': services,
			'fee':      fee,
			'expenses': expenses,
			'total':    0.0
		}

	calc_client_totals(project)

	return project

def load_clients(project, from_open = False):

	for name in project['edited']['clients']:

		client_default = project['default']['clients'][name]
		client = project['edited']['clients'][name]

		if name not in config.settings['clients']:
			config.new_client(name, from_open, client)

		settings = config.settings['clients'][name]

		# get client settings
		printed_name      = settings['printed_name']
		class_name        = settings['class']
		fee               = settings['fee']
		is_in_group       = settings['is_in_group']
		group_name        = settings['group_name']
		grouped_employees = settings['grouped_employees'].copy()

		# default data-----------------------------------------------------------
		# get project data
		date        = client_default['date']
		period      = client_default['period']
		memo        = client_default['memo']
		services    = client_default['services'].copy()
		project_fee = client_default['fee']
		expenses    = client_default['expenses'].copy()
		total       = client_default['total']

		# instantiate client
		client_default = clients.Client(
			name,
			date,
			period,
			memo,
			printed_name,
			class_name,
			fee,
			is_in_group,
			group_name,
			grouped_employees,
			services,
			project_fee,
			expenses,
			total,
			default = True
		)
		# default data-----------------------------------------------------------

		# edited data-----------------------------------------------------------
		# get project data
		date        = client['date']
		period      = client['period']
		memo        = client['memo']
		services    = client['services'].copy()
		project_fee = client['fee']
		expenses    = client['expenses'].copy()
		total       = client['total']

		# instantiate client
		client_instance = clients.Client(
			name,
			date,
			period,
			memo,
			printed_name,
			class_name,
			fee,
			is_in_group,
			group_name,
			grouped_employees,
			services,
			project_fee,
			expenses,
			total
		)
		# edited data-----------------------------------------------------------

