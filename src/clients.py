# clients.py - A module for keeping track of any clients encountered in a
#	               timesheet report.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# Client------------------------------------------------------------------------
class Client:

	defaults  = {}
	instances = {}
	groups    = {}

	@classmethod
	def get_list(cls):

		client_list = []

		for client in cls.instances:
			client_list.append(client)

		return client_list

	def __init__(
		self,
		name,
		date,
		period,
		memo,
		printed_name      = None,
		class_name        = '',
		fee               = 0.0,
		is_in_group       = False,
		group_name        = None,
		grouped_employees = [],
		services          = [],
		project_fee       = None,
		expenses          = [],
		project_total     = 0.0,
		default           = False
	):

		# set defaults
		if printed_name == None:
			printed_name = name

		if project_fee == None:
			project_fee = fee

		# keep track of instances
		if default == True:
			Client.defaults[name] = self
		else:
			Client.instances[name] = self

		# client settings
		self.__name         = name
		self.__printed_name = printed_name
		self.__class_name   = class_name
		self.__fee          = fee
		self.__is_in_group  = is_in_group
		self.__group_name   = group_name
		self.__grouped_employees = grouped_employees.copy()
		self.__expenses     = expenses.copy()

		# project data
		self.__date          = date
		self.__period        = period
		self.__memo          = memo
		self.__services      = services.copy()
		self.__project_fee   = project_fee
		self.__project_total = project_total

		# handle groups
		if self.__is_in_group == True:
			self.add_to_group(group_name)

	# Setters-------------------------------------------------------------------

	def set_printed_name(self, name):

		self.__printed_name = name

	def set_class_name(self, name):

		self.__class_name = name

	def set_fee(self, fee):

		self.__fee = fee

	def set_in_group(self, switch):

		self.__is_in_group = switch

	def set_date(self, date):

		self.__date = date

	def set_period(self, period):

		self.__period = period

	def set_memo(self, memo):

		self.__memo = memo

	def set_project_fee(self, project_fee):

		self.__project_fee = project_fee

	def set_services(self, services):

		self.__services = services.copy()

	def set_expenses(self, expenses):

		self.__expenses = expenses.copy()

	def set_project_total(self, project_total):

		self.__project_total = project_total

	# Setters-------------------------------------------------------------------

	# Getters-------------------------------------------------------------------

	def get_name(self):

		return self.__name

	def get_printed_name(self):

		return self.__printed_name

	def get_class_name(self):

		return self.__class_name

	def get_fee(self):

		return self.__fee

	def get_in_group(self):

		return self.__is_in_group

	def get_date(self):

		return self.__date

	def get_period(self):

		return self.__period

	def get_memo(self):

		return self.__memo

	def get_project_fee(self):

		return self.__project_fee

	def get_grouped_employees(self):

		return self.__grouped_employees.copy()

	def get_services(self):

		return self.__services.copy()

	def get_expenses(self):

		return self.__expenses.copy()

	def get_project_total(self):

		return self.__project_total

	def get_settings(self):

		settings = {
			'printed_name':      self.__printed_name,
			'class':             self.__class_name,
			'fee':               self.__project_fee,  # TODO: will this cause any issues?
			'is_in_group':       self.__is_in_group,
			'group_name':        self.__group_name,
			'grouped_employees': self.__grouped_employees.copy()
		}

		return settings

	def get_data(self):

		data = {
			'date':        self.__date,
			'period':      self.__period,
			'memo':        self.__memo,
			'services':    self.__services.copy(),
			'project_fee': self.__project_fee,
			'expenses':    self.__expenses.copy(),
			'total':       self.__project_total
		}

		return data

	# Getters-------------------------------------------------------------------

	# Methods-------------------------------------------------------------------

	def add_to_group(self, group_name):

		if group_name in Client.groups:
			Client.groups[group_name].append(self.get_name())
		else:
			Client.groups[group_name] = [self.get_name()]

	def update_values(self, values):

		self.set_date(values['date'])
		self.set_period(values['period'])
		self.set_memo(values['memo'])
		self.set_services(values['services'].copy())
		self.set_project_fee(values['fee'])
		self.set_expenses(values['expenses'].copy())
		self.set_project_total(values['total'])

	# Methods-------------------------------------------------------------------

# Client------------------------------------------------------------------------
