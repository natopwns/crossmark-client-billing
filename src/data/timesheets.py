# timesheets.py - Functions related to processing Kimai timesheet exports.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later

import config


# ---------
# CONSTANTS
# ---------

CLIENT   = 8
EMPLOYEE = 6
TIME     = 3
COST     = 4


# ---------
# FUNCTIONS
# ---------

def get_cost(value):

	try:
		cost = float(value)
	except:
		cost = float(value[5:-1])

	return cost

def error_check(number):

	value = 0
	try:
		value = float(number)
	except:
		value = 0

	return value

def sort_list(input_list):

	# sort by client, then employee
	sorted_list = sorted(input_list[1:], key=lambda row: row[CLIENT])
	sorted_list = sorted(sorted_list,    key=lambda row: row[EMPLOYEE])

	return sorted_list

def report_by_employee(sorted_list):

	report   = [['Employee', 'Client', 'Hours', 'Labor Cost']]
	employee = ''
	client   = ''
	total    = 0.0
	cost     = 0.0

	employee_changed = False
	client_changed   = False

	for row in sorted_list:

		if employee != row[EMPLOYEE]:
			employee_changed = True

		if client != row[CLIENT]:
			client_changed = True

		# record line
		if employee_changed == True or client_changed == True:

			if employee != '':

				total = round(total, 2)
				cost  = round(cost,  2)
				line  = [employee, client, total, cost]
				report.append(line)

				total = 0.0
				cost  = 0.0

			employee_changed = False
			client_changed   = False

		# get next section info
		employee = row[EMPLOYEE]
		client   = row[CLIENT]
		total   += error_check(row[TIME]) / 60 / 60
		cost    += get_cost(row[COST])

	# record last line
	total = round(total, 2)
	cost  = round(cost,  2)
	line  = [employee, client, total, cost]
	report.append(line)

	return report

def report_by_client(sorted_list):

	by_employee = report_by_employee(sorted_list)

	# sort by client
	by_employee = sorted(by_employee[1:], key=lambda row: row[1])

	report = [['Client', 'Employee', 'Hours', 'Labor Cost']]

	# switch employee and client columns
	for row in by_employee:
		line = [row[1], row[0], row[2], row[3]]
		report.append(line)

	return report

def report_by_class(sorted_list):

	by_client = report_by_client(sorted_list)

	clients = config.settings['clients']
	report = [['Class', 'Client', 'Employee', 'Hours', 'Labor Cost']]
	data   = []

	for row in by_client[1:]:

		if row[0] in clients:
			line = [ clients[row[0]]['class'] ] + row
		else:
			line = [''] + row

		data.append(line)

	report += sorted(data, key=lambda row: row[0])

	return report

def dict_by_client(report):

	by_client = {}

	# sort by client
	report = sorted(report[1:], key=lambda row: row[1])

	# add clients to dictionary
	for row in report:

		client = row[1]

		if client not in by_client:
			by_client[client] = []

	# store client data in dictionary
	for row in report:

		job      = ''
		employee = row[0]
		client   = row[1]
		hours    = row[2]
		total    = row[3]
		if hours > 0:
			rate = round(total / hours, 2)
		else:
			rate = 0.0

		if employee in config.settings['employees']:
			job = config.settings['employees'][employee]

		by_client[client].append([job, employee, hours, rate, total])  # blank string for description

	return by_client

def add_totals(report_type, report):

	if report_type == 'By Class':
		spacer = ['', '']
		h_pos = 3
		c_pos = 4
	else:
		spacer = ['']
		h_pos = 2
		c_pos = 3

	report_totals = report.copy()

	section = report[1][0]
	hours   = 0.0
	cost    = 0.0

	row_num = 0

	for row in report:

		row_num += 1

		# skip header row
		if type(row[h_pos]) == str:
			continue

		# record line
		if section != row[0]:

			line = ['Total: ' + section] + spacer + [hours, cost]
			report_totals.insert(row_num - 1, line)
			report_totals.insert(row_num, [''] + spacer + ['', '']) # blank line
			row_num += 2

			hours = 0.0
			cost  = 0.0
			section = row[0]

		hours += row[h_pos]
		cost  += row[c_pos]

	# record last line
	line = ['Total: ' + section] + spacer + [hours, cost]
	report_totals.append(line)

	return report_totals

def run_report(report_type, input_list):

	report_types = {
		'By Employee': report_by_employee,
		'By Client':   report_by_client,
		'By Class':    report_by_class,
	}

	sorted_list = sort_list(input_list)
	report      = report_types[report_type](sorted_list)
	r_totals    = add_totals(report_type, report)

	return r_totals

# reference implementation
def process(input_list):

	sorted_list = sort_list(input_list)

	# generate dictionary
	by_employee = report_by_employee(sorted_list)
	by_client   = dict_by_client(by_employee)

	return by_client

