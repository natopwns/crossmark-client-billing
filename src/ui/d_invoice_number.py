# d_invoice_number.py - Starting invoice number dialog.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import tkinter as tk
from tkinter import simpledialog
import ui.constants as uic


# -------
# CLASSES
# -------

# InvoiceDialog-----------------------------------------------------------------
class InvoiceDialog(tk.simpledialog.Dialog):

	def __init__(self, parent):

		self.invoice_number = 1

		# run parent init
		super().__init__(parent, 'Invoice number')

	def body(self, frame):

		frame.pack(
			fill = tk.BOTH,
			expand = True,
			padx = uic.PADX,
			pady = uic.PADY
		)

		self.label = tk.Label(
			frame,
			text = 'Enter starting invoice number:',
			wraplength = 400,
			justify = tk.LEFT
		)
		self.label.pack()

		self.entry_var = tk.StringVar()
		self.entry = tk.Entry(
			frame,
			textvariable = self.entry_var
		)
		self.entry.pack()

		return frame

	def ok_pressed(self):

		self.invoice_number = self.entry_var.get()

		if self.invoice_number.isdigit() != True:
			self.invoice_number = 1

		self.invoice_number = int(self.invoice_number)

		self.destroy()

	def buttonbox(self):

		self.ok_button = tk.Button(self, text='OK', width=5, command=self.ok_pressed)
		self.ok_button.pack(
			side="right",
			padx = uic.PADX,
			pady = uic.PADY
		)
		self.bind("<Return>", lambda event: self.ok_pressed())

# ClientsDialog-----------------------------------------------------------------
