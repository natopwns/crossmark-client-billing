# options_employee.py - Employee options GUI.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import tkinter as tk
import config
import ui.constants as uic
import ui.d_employees


# -------
# CLASSES
# -------

# OptionsFrame------------------------------------------------------------------
class OptionsFrame(tk.Frame):

	# --Init--------------------------------------------------------------------
	def __init__(self, container):

		# initialize tk.Tk
		super().__init__(container)
		self.container = container

		self.feedback = tk.StringVar()
		self.feedback.set('Ready.')

		# make columns reactive
		self.columnconfigure(0, weight = 1)

		# state
		self.current_employee = None

		# keep track of current row
		self.last_row = 0

		# employee button
		self.employee_button = tk.Button(
			self,
			text = 'Select employee...',
			width = 30,
			wraplength = 220,
			command = self.choose_employee
		)
		self.employee_button.grid(
			row = self.last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		self.last_row += 1

		# add options frame
		self.options = tk.LabelFrame(
			self,
			text = 'Employee Options'
		)
		self.options.grid(
			row = self.last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		# make options frame reactive
		self.rowconfigure(self.last_row, weight = 1)

		# make options frame columns reactive
		self.options.columnconfigure(0, weight = 1)
		self.options.columnconfigure(1, weight = 2)

		# Options Frame Start---------------------------------------------------

		options_last_row = 0

		# employee description label
		self.description_label = tk.Label(
			self.options,
			text = 'Description:'
		)
		self.description_label.grid(
			row = options_last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		# employee description entry
		self.description = tk.StringVar()
		self.employee_description = tk.Entry(
			self.options,
			textvariable = self.description
		)
		self.employee_description.grid(
			row = options_last_row,
			column = 1,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		options_last_row += 1

		# Options Frame End-----------------------------------------------------

		self.last_row += 1

		# back button
		self.back_button = tk.Button(
			self,
			text = 'Save & return',
			command = self.save_back
		)
		self.back_button.grid(
			row = self.last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		self.last_row += 1

		# feedback label
		self.lbl_output = tk.Label(
			self,
			textvariable = self.feedback
		)
		self.lbl_output.grid(
			row = self.last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		self.last_row += 1

	# --Init--------------------------------------------------------------------

	# --Methods-----------------------------------------------------------------
	# ----Callbacks-----------------------------------------------------------
	def choose_employee(self):

		self.feedback.set('Choosing employee...')

		dialog = ui.d_employees.EmployeesDialog(self, config.get_as_list('employees'), False)

		if dialog.employee != None:

			self.update_values()  # update config

			self.current_employee = dialog.employee[0]
			displayed_string      = self.current_employee

			# fit displayed string in button
			if len(self.current_employee) > 58:
					displayed_string = self.current_employee[0:58] + '...'

			self.employee_button.configure(text = displayed_string)

			self.get_employee_settings()

		self.feedback.set('Ready.')

	def save_back(self):

		self.update_values()  # update config
		config.save()         # write config

		self.container.destroy_frame(self, 'back')

	# ----Callbacks-----------------------------------------------------------

	# ----Widget Data---------------------------------------------------------
	def get_employee_settings(self):

		description = config.settings['employees'][self.current_employee]

		self.description.set(description)

	def update_values(self):

		if self.current_employee != None:
			config.set_employee_description(self.current_employee, self.description.get())

	# ----Widget Data---------------------------------------------------------

	# --Methods-----------------------------------------------------------------

# OptionsFrame------------------------------------------------------------------

