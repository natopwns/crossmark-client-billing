# options_client.py - Client options GUI.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import tkinter as tk
import config
import ui.constants as uic
import ui.d_clients
import ui.d_client_groups
import ui.d_employees


# -------
# CLASSES
# -------

# OptionsFrame------------------------------------------------------------------
class OptionsFrame(tk.Frame):

	# --Init--------------------------------------------------------------------
	def __init__(self, container):

		# initialize tk.Tk
		super().__init__(container)
		self.container = container

		self.feedback = tk.StringVar()
		self.feedback.set('Ready.')

		# make columns reactive
		self.columnconfigure(0, weight = 1)

		# state
		self.current_client = None

		# keep track of current row
		self.last_row = 0

		# client button
		self.client_button = tk.Button(
			self,
			text = 'Select client...',
			width = 30,
			wraplength = 220,
			command = self.choose_client
		)
		self.client_button.grid(
			row = self.last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		self.last_row += 1

		# add options frame
		self.options = tk.LabelFrame(
			self,
			text = 'Client Options'
		)
		self.options.grid(
			row = self.last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		# make options frame reactive
		self.rowconfigure(self.last_row, weight = 1)

		# make options frame columns reactive
		self.options.columnconfigure(0, weight = 1)
		self.options.columnconfigure(1, weight = 2)

		# Options Frame Start---------------------------------------------------

		options_last_row = 0

		# fee label
		self.fee_label = tk.Label(
			self.options,
			text = 'Fee:'
		)
		self.fee_label.grid(
			row = options_last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		# fee entry
		self.fee_var = tk.StringVar()
		self.fee_entry = tk.Entry(
			self.options,
			justify = tk.RIGHT,
			textvariable = self.fee_var
		)
		self.fee_entry.grid(
			row = options_last_row,
			column = 1,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		options_last_row += 1

		# printed name label
		self.name_label = tk.Label(
			self.options,
			text = 'Printed name:'
		)
		self.name_label.grid(
			row = options_last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		# printed name entry
		self.name_var = tk.StringVar()
		self.name_entry = tk.Entry(
			self.options,
			textvariable = self.name_var
		)
		self.name_entry.grid(
			row = options_last_row,
			column = 1,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		options_last_row += 1

		# class name label
		self.class_label = tk.Label(
			self.options,
			text = 'Class name:'
		)
		self.class_label.grid(
			row = options_last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		# class name entry
		self.class_var = tk.StringVar()
		self.class_entry = tk.Entry(
			self.options,
			textvariable = self.class_var
		)
		self.class_entry.grid(
			row = options_last_row,
			column = 1,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		options_last_row += 1

		# client group label
		self.group = tk.StringVar()
		self.group.set('Group: None')
		self.group_label = tk.Label(
			self.options,
			textvariable = self.group
		)
		self.group_label.grid(
			row = options_last_row,
			column = 0,
			columnspan = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		options_last_row += 1

		# client group button
		self.client_group_button = tk.Button(
			self.options,
			text = 'Select client group',
			command = self.choose_client_group
		)
		self.client_group_button.grid(
			row = options_last_row,
			column = 0,
			columnspan = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		options_last_row += 1

		# grouped employees button
		self.grouped_employees_button = tk.Button(
			self.options,
			text = 'Select employees to group',
			command = self.choose_grouped_employees
		)
		self.grouped_employees_button.grid(
			row = options_last_row,
			column = 0,
			columnspan = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		options_last_row += 1

		# Options Frame End-----------------------------------------------------

		self.last_row += 1

		# back button
		self.back_button = tk.Button(
			self,
			text = 'Save & return',
			command = self.save_back
		)
		self.back_button.grid(
			row = self.last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		self.last_row += 1

		# feedback label
		self.lbl_output = tk.Label(
			self,
			textvariable = self.feedback
		)
		self.lbl_output.grid(
			row = self.last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY
		)

		self.last_row += 1

	# --Init--------------------------------------------------------------------

	# --Methods-----------------------------------------------------------------
	# ----Callbacks-----------------------------------------------------------
	def choose_client(self):

		self.feedback.set('Choosing client...')

		dialog = ui.d_clients.ClientsDialog(self, 'Choose client', config.get_as_list('clients'))

		if dialog.client != None:

			self.update_values()  # update config

			self.current_client = dialog.client
			displayed_string    = self.current_client

			# fit displayed string in button
			if len(self.current_client) > 58:
					displayed_string = self.current_client[0:58] + '...'

			self.client_button.configure(text = displayed_string)

			self.get_client_settings()

		self.feedback.set('Ready.')

	def choose_client_group(self):

		self.feedback.set('Choosing group...')

		if self.current_client != None:

			dialog = ui.d_client_groups.ClientGroups(self, 'Choose group', config.get_as_list('groups'))

			if dialog.group != None:
				config.set_group(self.current_client, dialog.group)

			self.set_group_name(config.get_group_name(self.current_client))

		self.feedback.set('Ready.')

	def choose_grouped_employees(self):

		self.feedback.set('Choosing employees...')

		if self.current_client != None:

			dialog = ui.d_employees.EmployeesDialog(self, config.get_as_list('employees'), True)

			if dialog.employee != None:
				config.set_grouped_employees(self.current_client, dialog.employee)

		self.feedback.set('Ready.')

	def save_back(self):

		self.update_values()  # update config
		config.save()         # write config

		self.container.destroy_frame(self, 'back')

	# ----Callbacks-----------------------------------------------------------

	# ----Widget Data---------------------------------------------------------
	def set_group_name(self, name):

		if name == '':
			name = 'None'
		self.group.set('Group: ' + name)

	def get_client_settings(self):

		settings = config.settings['clients'][self.current_client]

		self.fee_var.set(self.format(settings['fee']))
		self.name_var.set(settings['printed_name'])
		self.class_var.set(settings['class'])
		self.set_group_name(settings['group_name'])

	def update_values(self):

		if self.current_client != None:

			fee          = self.fee_var.get()
			printed_name = self.name_var.get()
			class_name   = self.class_var.get()

			config.set_client_fee(self.current_client, fee)
			config.set_client_printed_name(self.current_client, printed_name)
			config.set_client_class(self.current_client, class_name)

	# ----Widget Data---------------------------------------------------------

	# ----Miscellaneous-------------------------------------------------------
	def format(self, number):

		display = '%.2f' % number

		return display

	# ----Miscellaneous-------------------------------------------------------

	# --Methods-----------------------------------------------------------------

# OptionsFrame------------------------------------------------------------------

