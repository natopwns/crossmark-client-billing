# d_reports.py - Report chooser dialog.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import tkinter as tk
from tkinter import simpledialog
import ui.constants as uic


# -------
# CLASSES
# -------

# ReportsDialog-----------------------------------------------------------------
class ReportsDialog(tk.simpledialog.Dialog):

	def __init__(self, parent):

		self.report = None
		self.reports = [
			'By Employee',
			'By Client',
			'By Class'
		]

		# run parent init
		super().__init__(parent, 'Choose a report')

	def body(self, frame):

		frame.pack(
			fill = tk.BOTH,
			expand = True,
			padx = uic.PADX,
			pady = uic.PADY
		)

		self.scroll_y = tk.Scrollbar(frame, orient = tk.VERTICAL)
		self.scroll_y.pack(
			side = 'right',
			fill = 'y'
		)

		self.scroll_x = tk.Scrollbar(frame, orient = tk.HORIZONTAL)
		self.scroll_x.pack(
			side = 'bottom',
			fill = 'x'
		)

		self.reports_list = tk.Listbox(
			frame,
			#width = 40,
			#height = 20,
			xscrollcommand = self.scroll_x.set,
			yscrollcommand = self.scroll_y.set
		)
		self.reports_list.pack(
			side = 'left',
			fill = 'both',
			expand = True
		)

		self.scroll_x['command'] = self.reports_list.xview
		self.scroll_y['command'] = self.reports_list.yview

		# add items to list
		i = 0
		for report in self.reports:
			self.reports_list.insert(i, report)
			i += 1

		return frame

	def ok_pressed(self):

		selection = self.reports_list.curselection()

		if selection != ():
			line = selection[0]
			self.report = self.reports[line]
			self.destroy()

	def cancel_pressed(self):

		self.destroy()

	def buttonbox(self):

		self.ok_button = tk.Button(self, text='OK', width=5, command=self.ok_pressed)
		self.ok_button.pack(
			side="right",
			padx = uic.PADX,
			pady = uic.PADY
		)
		cancel_button = tk.Button(self, text='Cancel', width=5, command=self.cancel_pressed)
		cancel_button.pack(
			side="left",
			padx = uic.PADX,
			pady = uic.PADY
		)
		self.bind("<Return>", lambda event: self.ok_pressed())
		self.bind("<Escape>", lambda event: self.cancel_pressed())

# ReportsDialog-----------------------------------------------------------------
