# project.py - Tkinter GUI formatting instructions.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import tkinter as tk
from tkinter.messagebox import CANCEL, askokcancel
import ui.constants as uic
import ui.d_clients
import ui.d_invoice_number
import main
import export


# -------
# CLASSES
# -------

# ResizingCanvas----------------------------------------------------------------
class ResizingCanvas(tk.Canvas):

	def __init__(self, parent, **kwargs):

		tk.Canvas.__init__(
			self,
			parent,
			highlightthickness = 0,
			#bg = 'gray',
			**kwargs
		)
		self.bind('<Configure>', self.on_resize)
		self.height = self.winfo_reqheight()
		self.width = self.winfo_reqwidth()

	def on_resize(self,event):

		self.width = event.width
		self.height = event.height
		# resize the canvas 
		#self.config(width=self.width, height=self.height)
		# rescale all the objects tagged with the "all" tag
		self.itemconfig('all', width = self.width - 1)

# ResizingCanvas----------------------------------------------------------------

# ProjectFrame------------------------------------------------------------------
class ProjectFrame(tk.Frame):

	# --Init--------------------------------------------------------------------
	def __init__(self, container, clients):

		# run parent init
		super().__init__(container)

		self.container = container
		self.clients   = clients

		self.feedback = tk.StringVar()
		self.feedback.set('Ready.')

		# make columns reactive
		self.columnconfigure(0, weight = 1)
		self.columnconfigure(1, weight = 1)
		self.columnconfigure(2, weight = 1)

		# state variables
		self.current_client    = None
		self.populating_client = True

		# keep track of current row
		last_row = 0

		# menu frame
		menu_frame = tk.Frame(
			self,
			bg = '#BABABA'
		)
		menu_frame.grid(
			row = last_row,
			column = 0,
			columnspan = 3,
			#padx = uic.PADX,
			#pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		# export menu
		self.export_menu = tk.Menubutton(
			menu_frame,
			text = 'File',
			bg = '#BABABA'
		)
		self.export_menu.grid(
			row = last_row,
			column = 0,
			sticky = tk.W
		)
		self.export_menu.menu = tk.Menu(
			self.export_menu,
			tearoff = 0
		)
		self.export_menu['menu'] = self.export_menu.menu
		# export to csv
		self.export_menu.menu.add_command(
			label = 'Export invoice to CSV',
			command = self.export_csv_single,
			state = tk.DISABLED
		)
		self.export_menu.menu.add_command(
			label = 'Export all to CSV',
			command = self.export_csv,
			state = tk.DISABLED
		)
		# export to iif
		self.export_menu.menu.add_command(
			label = 'Export invoice to IIF',
			command = self.export_iif_single,
			state = tk.DISABLED
		)
		self.export_menu.menu.add_command(
			label = 'Export all to IIF',
			command = self.export_iif,
			state = tk.DISABLED
		)
		# export to pdf
		self.export_menu.menu.add_command(
			label = 'Export invoice to PDF',
			command = self.export_pdf_single,
			state = tk.DISABLED
		)
		self.export_menu.menu.add_command(
			label = 'Export all to PDF',
			command = self.export_pdf,
			state = tk.DISABLED
		)

		last_row += 1

		# invoice frame
		self.invoice = tk.LabelFrame(
			self,
			text = 'Invoice'
		)
		self.invoice.grid(
			row = last_row,
			column = 0,
			columnspan = 3,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		# make invoice frame reactive
		self.rowconfigure(last_row, weight = 1)

		# configure invoice frame grid reactivity
		self.invoice.columnconfigure(0, weight = 1)
		self.invoice.columnconfigure(1, weight = 1)
		self.invoice.columnconfigure(2, weight = 1)
		self.invoice.columnconfigure(3, weight = 1)

		# Invoice Frame Begin---------------------------------------------------

		invoice_last_row = 0

		# client button
		self.client_button = tk.Button(
			self.invoice,
			text = 'Select client...',
			command = self.choose_client
		)
		self.client_button.grid(
			row = invoice_last_row,
			column = 0,
			columnspan = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		# date label
		date_label = tk.Label(
			self.invoice,
			text = 'Date:'
		)
		date_label.grid(
			row = invoice_last_row,
			column = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		# date entry
		self.date_var = tk.StringVar()
		self.date_entry = tk.Entry(
			self.invoice,
			textvariable = self.date_var
		)
		self.date_entry.grid(
			row = invoice_last_row,
			column = 3,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		invoice_last_row += 1

		# period label
		period_label = tk.Label(
			self.invoice,
			text = 'Period:'
		)
		period_label.grid(
			row = invoice_last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.W
		)
		# period entry
		self.period_var = tk.StringVar()
		self.period_entry = tk.Entry(
			self.invoice,
			textvariable = self.period_var
		)
		self.period_entry.grid(
			row = invoice_last_row,
			column = 1,
			columnspan = 3,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		invoice_last_row += 1

		# services provided label
		services_label = tk.Label(
			self.invoice,
			text = 'Services Provided:'
		)
		services_label.grid(
			row = invoice_last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.W
		)

		# services provided entry
		self.services_var = tk.StringVar()
		self.services_entry = tk.Entry(
			self.invoice,
			textvariable = self.services_var
		)
		self.services_entry.grid(
			row = invoice_last_row,
			column = 1,
			columnspan = 3,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		invoice_last_row += 1

		# services canvas frame
		self.services_cf = tk.LabelFrame(
			self.invoice,
			text = 'Professional Services'
		)
		self.services_cf.grid(
			row = invoice_last_row,
			column = 0,
			columnspan = 4,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		# make services frame reactive
		self.invoice.rowconfigure(invoice_last_row, weight = 1)

		# align headers with table columns
		self.services_cf.columnconfigure(0, weight = 3)
		self.services_cf.columnconfigure(1, weight = 3)
		self.services_cf.columnconfigure(2, weight = 1)
		self.services_cf.columnconfigure(3, weight = 1)
		self.services_cf.columnconfigure(4, weight = 1)

		# Services Canvas Frame Begin-----------------------------------------

		services_cf_last_row = 0

		# type label
		stl = tk.Label(self.services_cf, text = 'Type')
		stl.grid(row = services_cf_last_row, column = 0)
		# employee label
		sel = tk.Label(self.services_cf, text = 'Employee')
		sel.grid(row = services_cf_last_row, column = 1)
		# hours label
		shl = tk.Label(self.services_cf, text = 'Hours')
		shl.grid(row = services_cf_last_row, column = 2)
		# rate label
		srl = tk.Label(self.services_cf, text = 'Rate')
		srl.grid(row = services_cf_last_row, column = 3)
		# total label
		sll = tk.Label(self.services_cf, text = 'Total')
		sll.grid(row = services_cf_last_row, column = 4)

		services_cf_last_row += 1

		# services canvas scrollbar
		self.services_scroll_y = tk.Scrollbar(self.services_cf, orient = tk.VERTICAL)
		self.services_scroll_y.grid(
			row = services_cf_last_row,
			column = 5,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.N + tk.S
		)

		# services canvas
		self.services_canvas = ResizingCanvas(
			self.services_cf,
			yscrollcommand = self.services_scroll_y.set,
			width = 600,
			height = 210
		)
		self.services_canvas.grid(
			row = services_cf_last_row,
			column = 0,
			columnspan = 5,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)
		self.services_scroll_y['command'] = self.services_canvas.yview

		# make services canvas reactive
		self.services_cf.rowconfigure(services_cf_last_row, weight = 1)

		# services frame
		self.services = tk.LabelFrame(
			self.services_canvas
		)

		# make services table reactive
		self.services.columnconfigure(0, weight = 3)
		self.services.columnconfigure(1, weight = 3)
		self.services.columnconfigure(2, weight = 1)
		self.services.columnconfigure(3, weight = 1)
		self.services.columnconfigure(4, weight = 1)

		self.services.columnconfigure(0, weight = 1)

		# update canvas scrollable area when frame size changes
		self.services.bind('<Configure>', self.on_services_configure)

		# draw frame in canvas
		self.services_canvas.create_window((0, 0), window = self.services, anchor = tk.N + tk.W)

		self.services_canvas.addtag_all('all')

		# Services Frame Begin----------------------------------------------

		self.services_last_row = 0

		self.services_widgets = []  # store cell widgets
		self.services_table = []    # store tk stringvars for each cell in services

		self.add_employee_row()

		# Services Frame End------------------------------------------------

		services_cf_last_row += 1

		# total label
		self.services_total_label = tk.Label(
			self.services_cf,
			text = 'Total:'
		)
		self.services_total_label.grid(
			row = services_cf_last_row,
			column = 0,
			columnspan = 4,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		# total entry
		self.services_total_var = tk.StringVar()
		self.services_total_var.trace_add('write', self.update_final_total)
		self.services_total_entry = tk.Entry(
			self.services_cf,
			width = 8,
			justify = tk.RIGHT,
			textvariable = self.services_total_var,
			state = 'readonly'
		)
		self.services_total_entry.grid(
			row = services_cf_last_row,
			column = 4,
			columnspan = 2,
			padx = 0,
			pady = uic.PADY,
			sticky = tk.W
		)

		# Services Canvas Frame End-------------------------------------------

		invoice_last_row += 1

		# monthly fixed fee label
		mff_label = tk.Label(
			self.invoice,
			text = 'Monthly Fixed Fee:'
		)
		mff_label.grid(
			row = invoice_last_row,
			column = 0,
			columnspan = 3,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.W
		)
		# monthly fixed fee entry
		self.mff_var = tk.StringVar()
		self.mff_var.trace_add('write', self.update_final_total)
		self.mff_entry = tk.Entry(
			self.invoice,
			width = 10,
			justify = tk.RIGHT,
			textvariable = self.mff_var
		)
		self.mff_entry.grid(
			row = invoice_last_row,
			column = 3,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		invoice_last_row += 1

		# expenses canvas frame
		self.expenses_cf = tk.LabelFrame(
			self.invoice,
			text = 'Out of Pocket Expenses'
		)
		self.expenses_cf.grid(
			row = invoice_last_row,
			column = 0,
			columnspan = 4,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		# make expenses frame reactive
		self.invoice.rowconfigure(invoice_last_row, weight = 1)

		# align headers with table columns
		self.expenses_cf.columnconfigure(0, weight = 10)
		self.expenses_cf.columnconfigure(1, weight = 10)
		self.expenses_cf.columnconfigure(2, weight = 50)
		self.expenses_cf.columnconfigure(3, weight = 7)

		# Expenses Canvas Frame Begin-----------------------------------------

		expenses_cf_last_row = 0

		# type label
		etl = tk.Label(self.expenses_cf, text = 'Type')
		etl.grid(row = expenses_cf_last_row, column = 0, columnspan = 3)
		# total label
		ell = tk.Label(self.expenses_cf, text = 'Total')
		ell.grid(row = expenses_cf_last_row, column = 3, sticky = tk.E + tk.W)

		expenses_cf_last_row += 1

		# expenses canvas scrollbar
		self.expenses_scroll_y = tk.Scrollbar(self.expenses_cf, orient = tk.VERTICAL)
		self.expenses_scroll_y.grid(
			row = expenses_cf_last_row,
			column = 4,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.N + tk.S
		)

		# expenses canvas
		self.expenses_canvas = ResizingCanvas(
			self.expenses_cf,
			yscrollcommand = self.expenses_scroll_y.set,
			width = 600,
			height = 110
		)
		self.expenses_canvas.grid(
			row = expenses_cf_last_row,
			column = 0,
			columnspan = 4,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)
		self.expenses_scroll_y['command'] = self.expenses_canvas.yview

		# make expenses canvas reactive
		self.expenses_cf.rowconfigure(expenses_cf_last_row, weight = 1)

		# expenses frame
		self.expenses = tk.LabelFrame(
			self.expenses_canvas
		)
		self.expenses.columnconfigure(0, weight = 90)
		self.expenses.columnconfigure(1, weight = 1)

		# update canvas scrollable area when frame size changes
		self.expenses.bind('<Configure>', self.on_expenses_configure)

		# draw frame in canvas
		self.expenses_canvas.create_window((0, 0), window = self.expenses, anchor = tk.N + tk.W)

		self.expenses_canvas.addtag_all('all')

		# Expenses Frame Begin----------------------------------------------

		self.expenses_last_row = 0

		self.expenses_widgets = []  # store cell widgets
		self.expenses_table = []    # store tk stringvars for each cell in expenses

		self.add_expense_row()

		# Expenses Frame End------------------------------------------------

		expenses_cf_last_row += 1

		# add row button
		self.add_row_button = tk.Button(
			self.expenses_cf,
			text = 'Add row',
			command = self.add_expense_row
		)
		self.add_row_button.grid(
			row = expenses_cf_last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.W
		)

		# remove row button
		self.remove_row_button = tk.Button(
			self.expenses_cf,
			text = 'Remove last row',
			command = self.remove_expense_row
		)
		self.remove_row_button.grid(
			row = expenses_cf_last_row,
			column = 1,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.W
		)

		# total label
		expenses_total_label = tk.Label(
			self.expenses_cf,
			text = 'Total:'
		)
		expenses_total_label.grid(
			row = expenses_cf_last_row,
			column = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		# total entry
		self.expenses_total_var = tk.StringVar()
		self.expenses_total_var.trace_add('write', self.update_final_total)
		self.expenses_total_entry = tk.Entry(
			self.expenses_cf,
			width = 8,
			justify = tk.RIGHT,
			textvariable = self.expenses_total_var,
			state = 'readonly'
		)
		self.expenses_total_entry.grid(
			row = expenses_cf_last_row,
			column = 3,
			padx = 0,
			pady = uic.PADY,
			sticky = tk.W
		)

		# Expenses Canvas Frame End-------------------------------------------

		invoice_last_row += 1

		# final total label
		final_label = tk.Label(
			self.invoice,
			text = 'Final Total:'
		)
		final_label.grid(
			row = invoice_last_row,
			column = 0,
			columnspan = 3,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.W
		)

		# final total entry
		self.final_var = tk.StringVar()
		self.final_entry = tk.Entry(
			self.invoice,
			width = 10,
			justify = tk.RIGHT,
			textvariable = self.final_var
		)
		self.final_entry.grid(
			row = invoice_last_row,
			column = 3,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		invoice_last_row += 1

		# Invoice Frame End-----------------------------------------------------

		last_row += 1

		# back button
		self.btn_back = tk.Button(
			self,
			text = 'Back',
			command = self.back
		)
		self.btn_back.grid(
			row = last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.W
		)

		# revert button
		self.btn_revert = tk.Button(
			self,
			text = 'Revert client',
			command = self.revert
		)
		self.btn_revert.grid(
			row = last_row,
			column = 1,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E + tk.W
		)

		# save button
		self.btn_save = tk.Button(
			self,
			text = 'Save',
			command = self.save
		)
		self.btn_save.grid(
			row = last_row,
			column = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = tk.E
		)

		last_row += 1

		# feedback label
		self.lbl_output = tk.Label(
			self,
			textvariable = self.feedback
		)
		self.lbl_output.grid(
			row = last_row,
			column = 0,
			columnspan = 3,
			padx = uic.PADX,
			pady = uic.PADY
		)

		last_row += 1

		# clean up
		self.populating_client = False

	# --Init--------------------------------------------------------------------

	# --Methods-----------------------------------------------------------------
	# ----Callbacks-----------------------------------------------------------
	def export_csv_single(self):

		if self.current_client != None:

			self.feedback.set('Exporting...')

			invoice_number = self.get_invoice_number()
			export.csv.export(invoice_number, self.current_client)

			self.feedback.set('Exported.')

	def export_csv(self):

		self.feedback.set('Exporting...')

		invoice_number = self.get_invoice_number()
		export.csv.export(invoice_number)

		self.feedback.set('Exported.')

	def export_iif_single(self):

		if self.current_client != None:

			self.feedback.set('Exporting...')

			invoice_number = self.get_invoice_number()
			export.iif.export(invoice_number, self.current_client)

			self.feedback.set('Exported.')

	def export_iif(self):

		self.feedback.set('Exporting...')

		invoice_number = self.get_invoice_number()
		export.iif.export(invoice_number)

		self.feedback.set('Exported.')

	def export_pdf_single(self):

		if self.current_client != None:

			self.feedback.set('Exporting...')

			invoice_number = self.get_invoice_number()
			export.pdf.export(invoice_number, self.current_client)

			self.feedback.set('Exported.')

	def export_pdf(self):

		self.feedback.set('Exporting...')

		invoice_number = self.get_invoice_number()
		export.pdf.export(invoice_number)

		self.feedback.set('Exported.')

	def on_hours_changed(self, *args):

		if self.populating_client == False:
			location = self.find_var(args[0])
			self.calc_services_totals(location)

	def on_rate_changed(self, *args):

		if self.populating_client == False:
			location = self.find_var(args[0])
			self.calc_services_totals(location)

	def update_total_services(self, *args):

		if self.populating_client == False:
			self.get_total_services()

	def update_total_expenses(self, *args):

		if self.populating_client == False:
			self.get_total_expenses()

	def update_final_total(self, *args):

		if self.populating_client == False:
			self.get_final_total()

	def on_services_configure(self, event):

		self.services_canvas.configure(scrollregion = self.services_canvas.bbox('all'))

	def on_expenses_configure(self, event):

		self.expenses_canvas.configure(scrollregion = self.expenses_canvas.bbox('all'))

	def choose_client(self):

		self.feedback.set('Choosing client...')

		self.populating_client = True

		dialog = ui.d_clients.ClientsDialog(self.container, 'Choose client', self.clients.get_list())

		if dialog.client != None:

			# save current values to client object
			if self.current_client != None:
				self.clients.instances[self.current_client].update_values(self.get_entered_values())

			self.current_client = dialog.client
			displayed_string    = self.current_client

			client = self.clients.instances[self.current_client]

			if len(self.current_client) > 20:
				displayed_string = self.current_client[0:36] + '...'

			self.client_button.configure(text = displayed_string)

			self.adjust_rows(client)
			self.get_client_values(client)

			# reset canvas positions
			self.services_canvas.yview_moveto(0.0)
			self.expenses_canvas.yview_moveto(0.0)

		self.populating_client = False

		self.feedback.set('Ready.')

	def revert(self):

		choice = askokcancel(
			'Are you sure?',
			'This will revert all changes you have made to this client.',
			default = CANCEL)

		if choice == True:
			name = self.current_client
			if name != None:
				client_default = self.clients.defaults[name]
				self.adjust_rows(client_default)
				self.get_client_values(client_default)

	def back(self):

		choice = askokcancel(
			'Are you sure?',
			'Any unsaved changes will be lost.',
			default = CANCEL)

		if choice == True:
			self.container.destroy_frame(self, 'back')

	def save(self):

		self.feedback.set('Saving project...')

		# save current values to client object
		if self.current_client != None:
			self.clients.instances[self.current_client].update_values(self.get_entered_values())

		main.save_project()

		self.feedback.set('Saved.')

	# ----Callbacks-----------------------------------------------------------

	# ----Widget Management---------------------------------------------------
	def add_employee_row(self):

		cell_padding = 1

		type_var = tk.StringVar()
		type_field = tk.Entry(
			self.services,
			textvariable = type_var
		)
		type_field.grid(
			row = self.services_last_row,
			column = 0,
			padx = cell_padding,
			pady = cell_padding,
			sticky = 'nsew'
		)
		employee_var = tk.StringVar()
		employee_field = tk.Entry(
			self.services,
			textvariable = employee_var
		)
		employee_field.grid(
			row = self.services_last_row,
			column = 1,
			padx = cell_padding,
			pady = cell_padding,
			sticky = 'nsew'
		)
		hours_var = tk.StringVar()
		hours_var.trace_add('write', self.on_hours_changed)
		hours_field = tk.Entry(
			self.services,
			width = 8,
			justify = tk.RIGHT,
			textvariable = hours_var
		)
		hours_field.grid(
			row = self.services_last_row,
			column = 2,
			padx = cell_padding,
			pady = cell_padding,
			sticky = 'nsew'
		)
		rate_var = tk.StringVar()
		rate_var.trace_add('write', self.on_rate_changed)
		rate_field = tk.Entry(
			self.services,
			width = 8,
			justify = tk.RIGHT,
			textvariable = rate_var
		)
		rate_field.grid(
			row = self.services_last_row,
			column = 3,
			padx = cell_padding,
			pady = cell_padding,
			sticky = 'nsew'
		)
		total_var = tk.StringVar()
		total_var.trace_add('write', self.update_total_services)
		total_field = tk.Entry(
			self.services,
			width = 8,
			justify = tk.RIGHT,
			textvariable = total_var
		)
		total_field.grid(
			row = self.services_last_row,
			column = 4,
			padx = cell_padding,
			pady = cell_padding,
			sticky = 'nsew'
		)

		row         = [type_field, employee_field, hours_field, rate_field, total_field]
		string_vars = [type_var, employee_var, hours_var, rate_var, total_var]

		self.services_widgets.append(row)
		self.services_table.append(string_vars)

		self.services_last_row += 1

	def remove_employee_row(self):

		if self.services_last_row > 1:
			# don't know if I need to destroy the cells manually, but here it is for now
			row = self.services_widgets[-1]
			for widget in row:
				widget.destroy()

			self.services_table.pop(-1)  # I hope this deletes the StringVars, and doesn't mem leak
			self.services_widgets.pop(-1)
			self.services_last_row -= 1

	def add_expense_row(self):

		cell_padding = 1

		type_var = tk.StringVar()
		type_field = tk.Entry(
			self.expenses,
			width = 58,
			textvariable = type_var
		)
		type_field.grid(
			row = self.expenses_last_row,
			column = 0,
			padx = cell_padding,
			pady = cell_padding,
			sticky = 'nsew'
		)
		total_var = tk.StringVar()
		total_var.trace_add('write', self.update_total_expenses)
		total_field = tk.Entry(
			self.expenses,
			width = 8,
			justify = tk.RIGHT,
			textvariable = total_var
		)
		total_field.grid(
			row = self.expenses_last_row,
			column = 1,
			padx = cell_padding,
			pady = cell_padding,
			sticky = 'nsew'
		)

		row = [type_field, total_field]
		string_vars = [type_var, total_var]

		self.expenses_widgets.append(row)
		self.expenses_table.append(string_vars)

		self.expenses_last_row += 1

	def remove_expense_row(self):

		if self.expenses_last_row > 1:

			# don't know if I need to destroy the cells manually, but here it is for now
			row = self.expenses_widgets[-1]
			for widget in row:
				widget.destroy()

			self.expenses_table.pop(-1)  # I hope this deletes the StringVars, and doesn't mem leak
			self.expenses_widgets.pop(-1)
			self.expenses_last_row -= 1

			# update totals
			if self.populating_client == False:
				self.get_total_expenses()

	def adjust_rows(self, client):

		services_row_count = len(client.get_services())
		expenses_row_count = len(client.get_expenses())

		if services_row_count == 0:
			services_row_count += 1
		if expenses_row_count == 0:
			expenses_row_count += 1

		# add services rows
		if self.services_last_row < services_row_count:

			add_count = services_row_count - self.services_last_row
			for i in range(0, add_count):
				self.add_employee_row()

		# remove services rows
		elif self.services_last_row > services_row_count:

			remove_count = self.services_last_row - services_row_count
			for i in range(0, remove_count):
				self.remove_employee_row()

		# add expense rows
		if self.expenses_last_row < expenses_row_count:

			add_count = expenses_row_count - self.expenses_last_row
			for i in range(0, add_count):
				self.add_expense_row()

		# remove expense rows
		elif self.expenses_last_row > expenses_row_count:

			remove_count = self.expenses_last_row - expenses_row_count
			for i in range(0, remove_count):
				self.remove_expense_row()

	def get_invoice_number(self):

		dialog = ui.d_invoice_number.InvoiceDialog(self.container)

		return dialog.invoice_number

	# ----Widget Management---------------------------------------------------

	# ----Widget Data---------------------------------------------------------
	def get_entered_values(self):

		date     = self.date_var.get()
		period   = self.period_var.get()
		memo     = self.services_var.get()
		services = self.get_services_list()
		fee      = self.error_check(self.mff_var.get())
		expenses = self.get_expenses_list()
		total    = self.error_check(self.final_var.get())

		values = {
			'date':     date,
			'period':   period,
			'memo':     memo,
			'services': services,
			'fee':      fee,
			'expenses': expenses,
			'total':    total
		}

		return values

	def get_services_list(self):

		services_list = []

		for row in self.services_table:

			type_var = row[0].get()
			employee = row[1].get()
			hours    = self.error_check(row[2].get())
			rate     = self.error_check(row[3].get())
			total    = self.error_check(row[4].get())

			if total != 0:
				services_list.append([type_var, employee, hours, rate, total])

		return services_list

	def get_expenses_list(self):

		expenses_list = []

		for row in self.expenses_table:

			type_var = row[0].get()
			total    = self.error_check(row[1].get())

			if total != 0:
				expenses_list.append([type_var, total])

		return expenses_list

	def get_client_values(self, client):

		self.date_var.set(client.get_date())
		self.period_var.set(client.get_period())
		self.services_var.set(client.get_memo())

		# services
		services = client.get_services()

		if len(services) == 0:

			for cell in self.services_table[0]:
				cell.set('')

		else:

			i = 0
			for line in services:

				cell_count = 0

				for cell in self.services_table[i]:

					value = line[cell_count]

					if cell_count > 2:
						value = self.format(value)

					cell.set(value)
					cell_count += 1

				i += 1

		self.get_total_services()

		self.mff_var.set(self.format(client.get_project_fee()))

		# expenses
		expenses = client.get_expenses()

		if len(expenses) == 0:

			for cell in self.expenses_table[0]:
				cell.set('')

		else:

			i = 0
			for line in expenses:

				cell_count = 0

				for cell in self.expenses_table[i]:

					value = line[cell_count]

					if cell_count > 0:
						value = self.format(value)

					cell.set(value)
					cell_count += 1

				i += 1

		self.get_total_expenses()

		# get project total
		project_total = client.get_project_total()
		if project_total == 0.0:
			self.get_final_total()
		else:
			self.final_var.set(self.format(project_total))

	def find_var(self, var_name):

		location = None  # location in services table

		# loop through services table
		row_number = 0
		for row in self.services_table:

			# check hours and rate column in each row
			for item in row[2:4]:
				if var_name == item._name:  # compare trace name with tk var name in table
					location = row_number
					break

			row_number += 1

		return location

	def calc_services_totals(self, location):

			row = self.services_table[location]
			hours = self.error_check(row[2].get())
			rate  = self.error_check(row[3].get())
			total = self.format(hours * rate)
			row[4].set(total)

	def get_total_services(self):

		total = 0.0

		for row in self.services_table:
			total += self.error_check(row[-1].get())

		total = self.format(total)
		self.services_total_var.set(total)

	def get_total_expenses(self):

		total = 0.0

		for row in self.expenses_table:
			total += self.error_check(row[-1].get())

		total = self.format(total)
		self.expenses_total_var.set(total)

	def get_final_total(self):

		services = self.error_check(self.services_total_var.get())
		fee      = self.error_check(self.mff_var.get())
		expenses = self.error_check(self.expenses_total_var.get())

		total = self.format(services + fee + expenses)
		self.final_var.set(total)

	# ----Widget Data---------------------------------------------------------

	# ----Miscellaneous-------------------------------------------------------
	def error_check(self, number):

		value = 0
		try:
			value = float(number)
		except:
			value = 0

		return value

	def format(self, number):

		display = '%.2f' % number

		return display

	# ----Miscellaneous-------------------------------------------------------

	# --Methods-----------------------------------------------------------------

# ProjectFrame------------------------------------------------------------------

