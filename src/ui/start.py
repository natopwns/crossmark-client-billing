# start.py - Tkinter GUI formatting instructions.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import tkinter as tk
from tkinter.filedialog import askopenfilename
import ui.constants as uic
import ui.start_functions as functions


# -------
# CLASSES
# -------

# StartFrame--------------------------------------------------------------------
class StartFrame(tk.Frame):

	def __init__(self, container):

		# initialize tk.Tk
		super().__init__(container)
		self.container = container

		# styling
		#self.configure(bg = uic.BG)

		self.feedback = tk.StringVar()
		self.feedback.set('Ready.')

		# make columns reactive
		self.columnconfigure(0, weight = 1)
		self.columnconfigure(1, weight = 1)

		# keep track of current row
		self.last_row = 0

		# add open button
		self.btn_open = tk.Button(
			self,
			text = 'Open project',
			command = self.open_project
		)
		self.btn_open.grid(
			row = self.last_row,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		# add new button
		self.btn_new = tk.Button(
			self,
			text = 'New project',
			command = self.new_project
		)
		self.btn_new.grid(
			row = self.last_row,
			column = 1,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		self.last_row += 1

		# reporting button
		self.btn_report = tk.Button(
			self,
			text = 'Reporting',
			command = self.on_reporting_clicked
		)
		self.btn_report.grid(
			row = self.last_row,
			column = 0,
			columnspan = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		self.last_row += 1

		# add options frame
		self.options = tk.LabelFrame(
			self,
			text = 'Options'
		)
		self.options.grid(
			row = self.last_row,
			column = 0,
			columnspan = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		# make options frame reactive
		self.rowconfigure(self.last_row, weight = 1)

		# configure options frame grid reactivity
		self.options.columnconfigure(0, weight = 1)

		# Options Frame Start---------------------------------------------------

		# clients option button
		self.btn_clients = tk.Button(
			self.options,
			text = 'Clients',
			command = self.open_options_client
		)
		self.btn_clients.grid(
			row = 0,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		# employee option button
		self.btn_employees = tk.Button(
			self.options,
			text = 'Employees',
			command = self.open_options_employee
		)
		self.btn_employees.grid(
			row = 1,
			column = 0,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		# Options Frame End-----------------------------------------------------

		self.last_row += 1

		# feedback label
		self.lbl_output = tk.Label(
			self,
			textvariable = self.feedback
		)
		self.lbl_output.grid(
			row = self.last_row,
			column = 0,
			columnspan = 2,
			padx = uic.PADX,
			pady = uic.PADY,
			sticky = 'nsew'
		)

		self.last_row += 1

	def open_project(self):

		clients = functions.open_project(self)

		if clients != None:
			self.container.destroy_frame(self, 'open', clients)  # pass data to root and destroy self

	def new_project(self):

		# get path and save empty project
		clients = functions.new_project(self)

		# switch to project frame
		if clients != None:
			self.container.destroy_frame(self, 'open', clients)  # pass data to root and destroy self

	def on_reporting_clicked(self):

		functions.run_report(self)

	def open_options_client(self):

		self.container.destroy_frame(self, 'options_client')

	def open_options_employee(self):

		self.container.destroy_frame(self, 'options_employee')

# StartFrame--------------------------------------------------------------------

