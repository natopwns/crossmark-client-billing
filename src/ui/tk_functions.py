# tkinter_functions.py - Functions specific to the Tkinter GUI.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

from tkinter.filedialog import askopenfilename


# ---------
# FUNCTIONS
# ---------

def tk_pick_file(GUI):

	GUI.outputText.set('Opening File...')

	inputFile = askopenfilename(title="Select CSV", filetypes=[("CSV","*.csv"),("All files","*.*")])

	if type(inputFile) is tuple or inputFile == '' or inputFile[inputFile.rfind('.'):len(inputFile)] != '.csv':
		GUI.outputText.set('Please choose a CSV file.')
		return False

	return inputFile

