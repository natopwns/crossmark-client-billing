# project_functions.py - Functions specific to the Tkinter GUI.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import main


# ---------
# FUNCTIONS
# ---------

def save():

	main.save_project()

