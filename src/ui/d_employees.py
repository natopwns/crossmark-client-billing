# d_employees.py - Employee chooser dialog.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import tkinter as tk
from tkinter import simpledialog
import ui.constants as uic


# -------
# CLASSES
# -------

# EmployeesDialog---------------------------------------------------------------
class EmployeesDialog(tk.simpledialog.Dialog):

	def __init__(self, parent, employees, is_multi_select):

		self.employees   = employees
		self.employee    = None
		self.win_title   = 'Choose employee'
		self.select_mode = tk.BROWSE

		# get dialog title and select mode
		if is_multi_select:
			self.win_title   = 'Choose employee(s)'
			self.select_mode = tk.MULTIPLE

		# run parent init
		super().__init__(parent, self.win_title)

	def body(self, frame):

		frame.pack(
			fill = tk.BOTH,
			expand = True,
			padx = uic.PADX,
			pady = uic.PADY
		)

		self.scroll_y = tk.Scrollbar(frame, orient = tk.VERTICAL)
		self.scroll_y.pack(
			side = 'right',
			fill = 'y'
		)

		self.scroll_x = tk.Scrollbar(frame, orient = tk.HORIZONTAL)
		self.scroll_x.pack(
			side = 'bottom',
			fill = 'x'
		)

		self.employees_list = tk.Listbox(
			frame,
			width = 40,
			height = 20,
			selectmode = self.select_mode,
			xscrollcommand = self.scroll_x.set,
			yscrollcommand = self.scroll_y.set
		)
		self.employees_list.pack(
			side = 'left',
			fill = 'both',
			expand = True
		)

		self.scroll_x['command'] = self.employees_list.xview
		self.scroll_y['command'] = self.employees_list.yview

		# add items to list
		i = 0
		for employee in self.employees:
			self.employees_list.insert(i, employee)
			i += 1

		return frame

	def ok_pressed(self):

		self.employee = []  # return empty list if no employees selected

		selection = self.employees_list.curselection()

		if selection != ():

			i = 0
			for line_number in selection:
				employee = selection[i]
				self.employee.append(self.employees[employee])
				i += 1

		self.destroy()

	def cancel_pressed(self):

		self.destroy()

	def buttonbox(self):

		self.ok_button = tk.Button(self, text='OK', width=5, command=self.ok_pressed)
		self.ok_button.pack(
			side="right",
			padx = uic.PADX,
			pady = uic.PADY
		)
		cancel_button = tk.Button(self, text='Cancel', width=5, command=self.cancel_pressed)
		cancel_button.pack(
			side="left",
			padx = uic.PADX,
			pady = uic.PADY
		)
		self.bind("<Return>", lambda event: self.ok_pressed())
		self.bind("<Escape>", lambda event: self.cancel_pressed())

# EmployeesDialog---------------------------------------------------------------
