# d_client_groups.py - Client group chooser dialog.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import tkinter as tk
from tkinter import simpledialog
import ui.constants as uic
import config


# -------
# CLASSES
# -------

# ClientsGroups-----------------------------------------------------------------
class ClientGroups(tk.simpledialog.Dialog):

	def __init__(self, parent, title, groups):

		self.groups = ['None'] + groups
		self.group  = None

		# run parent init
		super().__init__(parent, title)

	def body(self, frame):

		frame.pack(
			fill = tk.BOTH,
			expand = True,
			padx = uic.PADX,
			pady = uic.PADY
		)

		self.scroll_y = tk.Scrollbar(frame, orient = tk.VERTICAL)
		self.scroll_y.pack(
			side = 'right',
			fill = 'y'
		)

		self.scroll_x = tk.Scrollbar(frame, orient = tk.HORIZONTAL)
		self.scroll_x.pack(
			side = 'bottom',
			fill = 'x'
		)

		self.groups_box = tk.Listbox(
			frame,
			width = 40,
			height = 10,
			xscrollcommand = self.scroll_x.set,
			yscrollcommand = self.scroll_y.set
		)
		self.groups_box.pack(
			side = 'left',
			fill = 'both',
			expand = True
		)

		self.scroll_x['command'] = self.groups_box.xview
		self.scroll_y['command'] = self.groups_box.yview

		# add items to list
		i = 1
		for group in self.groups:
			self.groups_box.insert(i, group)
			i += 1

		return frame

	def ok_pressed(self):

		selection = self.groups_box.curselection()
		if selection != ():
			group = selection[0]
			self.group = self.groups[group]
			self.destroy()

	def add_pressed(self):

		name = self.add_var.get()

		if name != 'Enter new group...':
			if name not in self.groups:
				self.groups_box.insert(tk.END, name)  # update gui
				self.groups.append(name)  # update list
				config.add_group(name)  # update config

	def delete_pressed(self):

		selection = self.groups_box.curselection()

		if selection != ():

			group_number = selection[0]
			group = self.groups[group_number]

			if group != 'None':

				# update dialog
				self.groups_box.delete(group_number)
				self.groups.remove(group)

				# delete group from config
				config.delete_group(group)

	def cancel_pressed(self):

		self.destroy()

	def buttonbox(self):

		self.add_var = tk.StringVar()
		self.add_var.set('Enter new group...')
		add_entry = tk.Entry(self, textvariable = self.add_var)
		add_entry.pack(
			side="top",
			expand = True,
			padx = uic.PADX,
			pady = uic.PADY
		)

		cancel_button = tk.Button(self, text='Cancel', width=5, command=self.cancel_pressed)
		cancel_button.pack(
			side="left",
			expand = True,
			padx = uic.PADX,
			pady = uic.PADY
		)

		delete_button = tk.Button(self, text='Delete', width=5, command=self.delete_pressed)
		delete_button.pack(
			side="left",
			expand = True,
			padx = uic.PADX,
			pady = uic.PADY
		)

		add_button = tk.Button(self, text='Add New', width=5, command=self.add_pressed)
		add_button.pack(
			side="left",
			expand = True,
			padx = uic.PADX,
			pady = uic.PADY
		)

		self.ok_button = tk.Button(self, text='OK', width=5, command=self.ok_pressed)
		self.ok_button.pack(
			side="left",
			expand = True,
			padx = uic.PADX,
			pady = uic.PADY
		)

		self.bind("<Return>", lambda event: self.ok_pressed())
		self.bind("<Escape>", lambda event: self.cancel_pressed())

# ClientsGroups-----------------------------------------------------------------

