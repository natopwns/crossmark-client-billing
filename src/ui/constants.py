# constants.py - Tkinter GUI formatting instructions.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# ---------
# CONSTANTS
# ---------

BG   = '#6E232E'
PADX = 8
PADY = 6
