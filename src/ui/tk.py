# tk.py - Tkinter GUI formatting instructions.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

import tkinter as tk
import ui.start
import ui.project
import ui.options_client
import ui.options_employee
import ui.d_error


# -------
# CLASSES
# -------

# GUI---------------------------------------------------------------------------
class GUI(tk.Tk):

	def __init__(self):

		# initialize tk.Tk
		super().__init__()

		# configure the master tk window
		self.title("Client Billing")
		#self.resizable(False, False)
		#self.configure(bg = '#6E232E')

		self.__create_widgets()

		# make grid reactive
		self.columnconfigure(0, weight = 1)
		self.rowconfigure(0, weight = 1)

	def __create_widgets(self):

		self.create_start_frame()

	def create_start_frame(self):

		frame = ui.start.StartFrame(self)
		frame.grid(
			row = 0,
			column = 0,
			sticky = 'nsew'
		)

	def create_project_frame(self, data):

		frame = ui.project.ProjectFrame(self, data)
		frame.grid(
			row = 0,
			column = 0,
			sticky = 'nsew'
		)

	def create_options_client_frame(self):

		frame = ui.options_client.OptionsFrame(self)
		frame.grid(
			row = 0,
			column = 0,
			sticky = 'nsew'
		)

	def create_options_employee_frame(self):

		frame = ui.options_employee.OptionsFrame(self)
		frame.grid(
			row = 0,
			column = 0,
			sticky = 'nsew'
		)

	def destroy_frame(self, frame, action, data = None):

		frame.destroy()

		self.geometry('')  # force window to resize to new frame

		if action == 'open':

			self.create_project_frame(data)

		elif action == 'back':

			self.create_start_frame()

		elif action == 'options_client':

			self.create_options_client_frame()

		elif action == 'options_employee':

			self.create_options_employee_frame()

	def report_callback_exception(self, error_class, error_string, traceback):

		error_dialog = ui.d_error.ErrorDialog(self, error_string)

		self.destroy()

# GUI---------------------------------------------------------------------------

