# start_functions.py - Functions specific to the Tkinter GUI.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


# -------
# IMPORTS
# -------

from tkinter.filedialog import askopenfilename
from tkinter.filedialog import askdirectory
import main
import ui.d_reports


# ---------
# FUNCTIONS
# ---------

def open_project(GUI):

	clients = None

	GUI.feedback.set('Opening File...')

	path = askopenfilename(title="Select JSON file", filetypes=[("JSON","*.json"),("All files","*.*")])

	if type(path) is tuple or path == '' or path[path.rfind('.'):len(path)] != '.json':
		GUI.feedback.set('Please choose a JSON file.')
	else:
		clients = main.open_project(path)

	return clients

def new_project(GUI):

	clients = None

	GUI.feedback.set('Getting project directory...')
	project_path = askdirectory(title = 'Select a directory to save new project')

	if type(project_path) is tuple or project_path == '':

		GUI.feedback.set('Please choose a directory.')

	else:

		GUI.feedback.set('Getting timesheet...')
		timesheet_path = askopenfilename(title="Select CSV file", filetypes=[("CSV","*.csv"),("All files","*.*")])

		if type(timesheet_path) is tuple or timesheet_path == '' or timesheet_path[timesheet_path.rfind('.'):len(timesheet_path)] != '.csv':
			GUI.feedback.set('Please choose a CSV file.')
		else:
			clients = main.new_project(project_path, timesheet_path)

	return clients

def run_report(GUI):

	# load timesheet
	GUI.feedback.set('Getting timesheet...')

	timesheet      = None
	timesheet_path = askopenfilename(title="Select CSV file", filetypes=[("CSV","*.csv"),("All files","*.*")])

	# return if no file chosen
	if type(timesheet_path) is tuple or timesheet_path == '' or timesheet_path[timesheet_path.rfind('.'):len(timesheet_path)] != '.csv':

		GUI.feedback.set('Please choose a CSV file.')
		return

	timesheet = main.load_timesheet(timesheet_path)

	# show report chooser
	dialog = ui.d_reports.ReportsDialog(GUI)

	# return if no report type chosen
	if dialog.report == None:
		GUI.feedback.set('Please choose a report type.')
		return

	# get path to save report
	GUI.feedback.set('Getting report directory...')
	report_path = askdirectory(title = 'Select a directory to save report')

	# return if no path chosen
	if type(report_path) is tuple or report_path == '':
		GUI.feedback.set('Please choose a directory.')
		return

	# run report
	report = main.run_report(dialog.report, report_path, timesheet)

	GUI.feedback.set('Report saved.')

