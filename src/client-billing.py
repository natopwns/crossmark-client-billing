# client-billing.py - Generate invoices from Kimai timesheet data.

# Copyright (C) 2022  Nathan Taylor

# Client-Billing is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Client-Billing is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Client-Billing.  If not, see <https://www.gnu.org/licenses/>.


# -------
# IMPORTS
# -------

import ui.tk
import config


# -----------------
# PROGRAM EXECUTION
# -----------------

# start gui
gui = ui.tk.GUI()
gui.mainloop()

