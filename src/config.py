# config.py - Handling config files.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


import os
import fileops.csv
import fileops.json


# constants
CONFIG_PATH = 'config.json'

# module variables
settings = {}

# Functions---------------------------------------------------------------------
# --Init----------------------------------------------------------------------
def init():

	global settings

	if os.path.exists(CONFIG_PATH):
		settings = load()
	else:
		settings = new()
		save()

# --Init----------------------------------------------------------------------

# --Setters-------------------------------------------------------------------
def set_group(client, group):

	global settings

	client_settings = settings['clients'][client]

	old_group = client_settings['group_name']

	# don't do anything if new group is the same
	if group == old_group:
		return

	if old_group != '':
		settings['groups'][old_group].remove(client)

	if group == 'None':

		client_settings['is_in_group'] = False
		client_settings['group_name']  = ''

	else:

		client_settings['is_in_group'] = True
		client_settings['group_name']  = group

		if group not in settings['groups']:
			settings['groups'][group] = []

		settings['groups'][group].append(client)

def set_client_fee(client_name, fee):

	global settings

	client = settings['clients'][client_name]
	client['fee'] = validate_number(fee)

def set_client_printed_name(client_name, printed_name):

	global settings

	client = settings['clients'][client_name]
	client['printed_name'] = validate_string(printed_name)

def set_client_class(client_name, class_name):

	global settings

	client = settings['clients'][client_name]
	client['class'] = validate_string(class_name)

def set_grouped_employees(client_name, employees):

	global settings

	client = settings['clients'][client_name]
	client['grouped_employees'] = employees

def set_employee_description(name, description):

	global settings

	settings['employees'][name] = validate_string(description)

# --Setters-------------------------------------------------------------------

# --Getters-------------------------------------------------------------------
def get_as_list(category):

	global settings

	items = []

	for item in settings[category]:
		items.append(item)

	return items

def get_group_name(client):

	global settings

	group_name = settings['clients'][client]['group_name']

	if group_name == '':
		group_name = 'None'

	return group_name

# --Getters-------------------------------------------------------------------

# --I/O-----------------------------------------------------------------------
def load():

	settings = fileops.json.read(CONFIG_PATH)

	return settings

def save():

	global settings

	fileops.json.write(settings, CONFIG_PATH)

# --I/O-----------------------------------------------------------------------

# --Other---------------------------------------------------------------------
def new():

	settings = {
		'clients':   {},  # client settings
		'employees': {},  # employee types
		'groups':    {}   # client groups
	}

	return settings

def new_client(name, data_from_project = False, data = {}):

	global settings

	if data_from_project:

		# get fee and expenses from project
		settings['clients'][name] = {
			'printed_name':      name,
			'class':             '',
			'fee':               data['fee'],
			'is_in_group':       False,
			'group_name':        '',
			'grouped_employees': [],
			'expenses':          data['expenses'].copy()
		}
	
	else:

		settings['clients'][name] = {
			'printed_name':      name,
			'class':             '',
			'fee':               0.0,
			'is_in_group':       False,
			'group_name':        '',
			'grouped_employees': [],
			'expenses':          []
		}

def update_client(name, client_settings, client_data):

	global settings

	settings['clients'][name] = {
		'printed_name':      client_settings['printed_name'],
		'class':             client_settings['class'],
		'fee':               client_settings['fee'],
		'is_in_group':       client_settings['is_in_group'],
		'group_name':        client_settings['group_name'],
		'grouped_employees': client_settings['grouped_employees'].copy(),
		'expenses':          client_data['expenses'].copy()
	}

def update_employees(services):

	global settings

	for row in services:

		type_var = row[0]
		name     = row[1]

		# add employee, if new
		if name not in settings['employees']:
			settings['employees'][name] = type_var

		# overwrite description, if new one found
		if type_var != '':
			settings['employees'][name] = type_var

def add_group(group):

	global settings

	if group not in settings['groups']:
		settings['groups'][group] = []

def delete_group(group_name):

	global settings

	group = settings['groups'][group_name].copy()

	for client in group:
		set_group(client, 'None')
	
	settings['groups'].pop(group_name)

def validate_number(number):

	try:
		number = round(float(number), 2)
	except:
		number = 0.0

	return number

def validate_string(string):

	try:
		string = str(string)
	except:
		string = ''

	return string

# --Other---------------------------------------------------------------------

# Functions---------------------------------------------------------------------

# Execution---------------------------------------------------------------------
init()

# Execution---------------------------------------------------------------------

