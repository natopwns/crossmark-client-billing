# iif.py - Export client instances to IIF's.

# Copyright (C) 2022  Nathan Taylor
# GPL-3.0-or-later


import fileops.csv
from clients import Client as clients


# constants
SERVICE = 0
EXPENSE = 1

# Functions---------------------------------------------------------------------
def get_client_data(client_name):

	client = {}

	client['settings'] = clients.instances[client_name].get_settings()
	client['data']     = clients.instances[client_name].get_data()

	return client

def add_iif_headers():

	headers = [
		[
			'!TRNS',
			'TRNSID',
			'TRNSTYPE',
			'DATE',
			'ACCNT',
			'NAME',
			'CLASS',
			'AMOUNT',
			'DOCNUM',
			'MEMO',
			'CLEAR',
			'TOPRINT',
			'NAMEISTAXABLE',
			'ADDR1',
			'ADDR3',
			'DUEDATE'
		],
		[
			'!SPL',
			'SPLID',
			'TRNSTYPE',
			'DATE',
			'ACCNT',
			'NAME',
			'CLASS',
			'AMOUNT',
			'DOCNUM',
			'MEMO',
			'CLEAR',
			'QNTY',
			'PRICE',
			'INVITEM',
			'TAXABLE',
			'SERVICEDATE'
		],
		['!ENDTRNS']
	]

	return headers

def add_line_transaction(name, client, invoice_number):

	TRNSID        = 1                        # split number, always 1
	TRNSTYPE      = 'INVOICE'                # fixed
	DATE          = client['data']['date']
	ACCNT         = ''                       # TODO
	NAME          = name
	CLASS         = ''                       # TODO
	AMOUNT        = client['data']['total']
	DOCNUM        = invoice_number           # invoice number
	MEMO          = ''                       # always blank
	CLEAR         = 'N'                      # fixed
	TOPRINT       = 'N'                      # fixed
	NAMEISTAXABLE = 'N'                      # fixed
	ADDR1         = ''                       # always blank
	ADDR3         = ''                       # always blank
	DUEDATE       = DATE                     # same as date

	line = [
		'TRNS',
		TRNSID,
		TRNSTYPE,
		DATE,
		ACCNT,
		NAME,
		CLASS,
		AMOUNT,
		DOCNUM,
		MEMO,
		CLEAR,
		TOPRINT,
		NAMEISTAXABLE,
		ADDR1,
		ADDR3,
		DUEDATE
	]

	return line

def add_line_split(client, split, split_count, split_type):

	date_string = client['data']['date']

	if split_type == SERVICE:
		amount = split[4]
		memo   = "{name} - {desc} ({date})".format(name = split[1], desc = split[0], date = date_string)
		qnty   = split[2]
		price  = split[3]
	elif split_type == EXPENSE:
		amount = split[1]
		memo   = "{desc} ({date})".format(desc = split[0], date = date_string)
		qnty   = ''
		price  = ''

	SPLID         = split_count  # split number, generated
	TRNSTYPE      = 'INVOICE'    # fixed
	DATE          = date_string
	ACCNT         = ''           # TODO
	NAME          = ''           # always blank
	CLASS         = ''           # TODO
	AMOUNT        = amount       # total
	DOCNUM        = ''           # always blank
	MEMO          = memo
	CLEAR         = 'N'          # fixed
	QNTY          = qnty         # hours
	PRICE         = price        # rate
	INVITEM       = ''           # TODO
	TAXABLE       = 'N'          # fixed
	SERVICEDATE   = ''           # always blank

	line = [
		'SPL',
		SPLID,
		TRNSTYPE,
		DATE,
		ACCNT,
		NAME,
		CLASS,
		AMOUNT,
		DOCNUM,
		MEMO,
		CLEAR,
		QNTY,
		PRICE,
		INVITEM,
		TAXABLE,
		SERVICEDATE
	]

	return line

def end_transaction():

	line = ['ENDTRNS']

	return line

def add_transaction(output, name, invoice_number):

	client = get_client_data(name)

	output.append(add_line_transaction(name, client, invoice_number))

	split_count = 2  # continue from TRNSID

	# fee
	split = ['Monthly Fixed Fee', client['data']['project_fee']]
	output.append(add_line_split(client, split, split_count, EXPENSE))
	split_count += 1

	# services
	for split in client['data']['services']:
		output.append(add_line_split(client, split, split_count, SERVICE))
		split_count += 1

	# expenses
	for split in client['data']['expenses']:
		output.append(add_line_split(client, split, split_count, EXPENSE))
		split_count += 1

	output.append(end_transaction())

def export(invoice_number, client_name = None):

	output = add_iif_headers()

	if client_name != None:

		add_transaction(output, client_name, invoice_number)

	else:

		for name in clients.instances:
			add_transaction(output, name, invoice_number)
			invoice_number += 1

	fileops.csv.write(output, 'project_test')

# Functions---------------------------------------------------------------------

